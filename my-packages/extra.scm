(define-module (my-packages extra)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages avahi)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages check)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages base)
  #:use-module (gnu packages java)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages scanner)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages maven)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages image)
  #:use-module (guix git-download)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages databases)
  #:use-module (guix build-system maven)
  #:use-module (guix build utils)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system ant)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages curl)
  #:use-module (guix build-system texlive)
  #:use-module (guix svn-download)
  #:use-module (gnu packages python))





(define-public gtklp
  (package
    (name "gtklp")
    (version "1.3.4")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://sourceforge.net/projects/gtklp/files/gtklp/"
                                  version
                                  "/gtklp-"
                                  version
                                  ".src.tar.gz"))
              (sha256 (base32 "1arvnnvar22ipgnzqqq8xh0kkwyf71q2sfsf0crajpsr8a8601xy"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:configure-flags
       (list
        "--disable-nls")
       #:phases
       (modify-phases %standard-phases
                      (add-after 'patch-source-shebangs 'fix
                                 (lambda _
                                   (let ((file "po/Makefile.in.in")
                                         (sh (string-append (assoc-ref %build-inputs "bash")
                                                            "/bin/bash")))
                                     (substitute* file
                                                  (("SHELL = /bin/sh")
                                                   (string-append "SHELL = "
                                                                  sh "\n"))))
                                   #true)))
       ))
    (inputs
     `(("glib" ,glib)
       ("lbzip2" ,lbzip2)
       ("cups" ,cups)
       ("gtk+-2" ,gtk+-2)
       ("automake" ,automake)
       ("autobuild" ,autobuild)
       ("autoconf" ,autoconf)
       ("bash" ,bash)
       ("m4" ,m4)
       ("pkg-config" ,pkg-config)
       ))
    (home-page "https://sourceforge.net/projects/gtklp/")
    (synopsis "Graphical Frontend to CUPS")
    (description "GtkLP is a graphical Frontend to CUPS. It is intended to be easy to use, small and it should compile on many UniX-Systems.")
    (license gpl3+)
    )
  )




(define-public my-hplip
  (package
    (inherit hplip)
    (name "my-hplip")
    (inputs
     `(("cups-minimal" ,cups-minimal)
       ("dbus" ,dbus)
       ("libjpeg" ,libjpeg-turbo)
       ("libusb" ,libusb)
       ("python" ,python)
       ("python-dbus" ,python-dbus)
       ("python-pygobject" ,python-pygobject)
       ("python-pyqt" ,python-pyqt)
       ("python-wrapper" ,python-wrapper)
       ("sane-backends" ,sane-backends-minimal)
       ("zlib" ,zlib)
       ("python-distro" ,python-distro)))))


(define-public texlive-generic-babel-french
  (package
    (name "texlive-generic-babel-french")
    (version (number->string %texlive-revision))
    (source (origin
              (method svn-fetch)
              (uri (texlive-ref "generic" "babel-french"))
              (file-name (string-append name "-" version "-checkout"))
              (sha256
               (base32
                "00374k4cqak1dvfmqs5fa61cq3qr5070nnv7mfvinc6kqn19md26"))))
    (build-system texlive-build-system)
    (arguments '(#:tex-directory "generic/babel-french"))
    (home-page "https://www.ctan.org/pkg/babel-french")
    (synopsis "Babel support for French")
    (description
     "This package provides the language definition file for support of
French in @code{babel}.")
    (license gpl3+)))


(define-public libiec61850
  (let* ((myversion "v1.5.0"))
    (package
     (name "libiec61850")
     (version myversion)
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mz-automation/libiec61850.git")
                    (commit myversion)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0rih8nz6svkm8r73xq7jxrcfnbfrfzlm8k8drszcrbx85sgrs3kr"))
              ))
     (build-system cmake-build-system)
     (home-page "https://github.com/mz-automation/libiec61850.git")
     (synopsis "IEC-61850 C library with python bindings")
     (description
      "libiec61850 is an open-source (GPLv3) implementation of an IEC 61850 client and server library implementing the protocols MMS, GOOSE and SV.")
     (license gpl3+))))
