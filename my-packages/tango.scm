(define-module (my-packages tango)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages avahi)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages check)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages base)
  #:use-module (gnu packages java)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages scanner)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages maven)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages image)
  #:use-module (guix git-download)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages databases)
  #:use-module (guix build-system maven)
  #:use-module (guix build utils)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system ant)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages curl)
  #:use-module (guix build-system texlive)
  #:use-module (guix svn-download)
  #:use-module (gnu packages python))




(define-public omniORB
  (package
   (name "omniORB")
   (version "4.2.3")
   (source (origin
             (method url-fetch)
             (uri (string-append "https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-"
                                 version
                                 "/omniORB-"
                                 version
                                 ".tar.bz2/download"))
             (sha256
              (base32
               "1jlb0wps6311dmhnphn64gv46z0bl8grch4fd9dcx5dlib02lh96"))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f
      #:configure-flags
      (list
       "--disable-thread-tracing"
       "--with-openssl"
       "--disable-ipv6"
       "--disable-static"
       )
      #:validate-runpath? #f
      #:make-flags
      (list
       (string-append "LDFLAGS=-Wl,-rpath="
                      (assoc-ref %outputs "out") "/lib"))))
   (inputs `(("python" ,python)
             ("pkg-config" ,pkg-config)
             ("openssl" ,openssl-1.0)))
   (synopsis "Corba library")
   (description
    "omniORB is an Object Request Broker (ORB) which implements
specification 2.6 of the Common Object Request Broker Architecture
(CORBA).")
   (home-page "http://omniorb.sourceforge.net")
   (license gpl3+)))

(define-public tangoidl
  (let ((commit "ada81f822b9d24c0bb2d7d250896e9521368f681"))
    (package
      (name "tangoidl")
      (version "5.1.0")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/tango-idl.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "1k134irvrkrbj43rqlai8ryih3548fw4q7v9ppmh6bdkql1s0pxm"))
                ))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f
         ;#:validate-runpath? #f
         ))
      (inputs
       `(
         ("omniorb" ,omniORB)
         ("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - idl")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+))))

(define-public tangoidl-java
  (let ((commit "ada81f822b9d24c0bb2d7d250896e9521368f681"))
    (package
      (name "tangoidl-java")
      (version "5.1.0")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/tango-idl.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "1k134irvrkrbj43rqlai8ryih3548fw4q7v9ppmh6bdkql1s0pxm"))
                ))
      (build-system maven-build-system)
      (arguments
       `(#:tests? #f
         ;#:validate-runpath? #f
         ))
      (inputs
       `(
         ("omniorb" ,omniORB)
         ("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - idl")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+))))



(define-public ccpTango
  (let ((commit "ef0c2be4a3223cbdbcd9521fc66eb8d7363cca87"))
    (package
      (name "ccpTango")
      (version "9.3.4")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/cppTango.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "1kzizvcgfs2pizwmb4vgqc8ylb95cf55h6i143s7v99zlz09f670"))
                ))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f
         #:configure-flags
         (list
          (string-append "-DIDL_BASE="
                         (assoc-ref %build-inputs "tangoidl")))
         ;; #:phases
         ;; (modifytphases %standardtphases
         ;;   (addtafter 'build 'pkg
         ;;     (lambda _
         ;;       (invoke (string-append "pkg-config --variable="
         ;;                              (assoc-ref %outputs "out")
         ;;                              " tango"))
         ;;       #true)))
         ;; #:test-target "run-tests"
         ))
      (inputs
       `(("python" ,python)
         ("python2" ,python-2.7)
         ("omniorb" ,omniORB)
         ("tangoidl" ,tangoidl)
         ("curl" ,curl)
         ("glibc" ,glibc)
         ("zeromq" ,zeromq)
         ("cppzmq" ,cppzmq)))
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - shared library")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+))))


(define-public TangoDatabase
  (let ((commit "3482ed34f16465ac125809166b70dcd5ebd73cf6"))
    (package
      (name "TangoDatabase")
      (version "5.16")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/TangoDatabase.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "0ys05amrb5wgcbzipy32yjpxb7rhyzwnm5m00p5vh5f12lv2m6b3"))
                ))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f
         #:configure-flags
         (list
          (string-append "-DMYSQL_INCLUDE_DIR="
                         (assoc-ref %build-inputs "mariadbdev") "/include/mysql")
          "-DMYSQL=mariadb"
          "-DMYSQL_ADMIN=root"
          "-DTANGO_DB_NAME=tango"
          "-DMYSQL_HOST=localhost"
          (string-append "-DTANGO_PKG_INCLUDE_DIRS="
                         (assoc-ref %build-inputs "ccpTango")
                         "/include/tango")
          (string-append "-DTANGO_PKG_LIBRARY_DIRS="
                         (assoc-ref %build-inputs "ccpTango")
                         "/lib")
          (string-append "-DPKG_CONFIG_PATH="
                         (assoc-ref %build-inputs "ccpTango")
                         "/lib/pkgconfig:"
                         (assoc-ref %build-inputs "mariadblib")
                         "/lib/pkgconfig")
          )
         )

       )
      (inputs
       `(("ccpTango" ,ccpTango)
         ("mariadbdev" ,mariadb "dev")
         ("mariadblib" ,mariadb "lib")
         ("mariadbout" ,mariadb "out")
         ("omniorb" ,omniORB)
         ("glibc" ,glibc)
         ("zeromq" ,zeromq)
         ("cppzmq" ,cppzmq)))
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - Tango Database")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+)
      )))

(define-public jtango
  (let ((commit "1b88808abf6a80697ede62b027594a1626d8a7f4"))
    (package
     (name "jtango")
     (version "9.6.8")
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/tango-controls/JTango.git")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0fihph91jczff7448xyncirqwvyr5lc0iimch8imvhl327k4wyzg"))
              ))
     (arguments
      `(#:exclude
        (list
         ("org.apache.maven.plugins" . ("maven-deploy-plugin" "maven-release-plugin"))
         ("org.codehaus.mojo" . ("buildnumber-maven-plugin"))
         )
        ))
     (build-system maven-build-system)
     (inputs
      `(
        ("maven-plugin-annotations" ,maven-plugin-annotations)
        ("tangoidl" ,tangoidl)
        ;("openjdk" ,openjdk9)
        ;("maven-install-plugin" ,maven-install-plugin)
        ))
     (synopsis "TANGO distributed control system - JTango")
     (description
      "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
     (home-page "http://www.tango-controls.org/")
     (license gpl3+)
     )))


(define-public maven-deploy-plugin
  (package
    (name "maven-deploy-plugin")
    (version "3.0.0-M1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://dlcdn.apache.org/maven/plugins/maven-deploy-plugin-"
                                  version
                                  "-source-release.zip"))
              (sha256 (base32 "1z7jz28y0v8wjvkygjzh0q92sn07f3gklzg9jp9z1sh2gdx59hwp"))))
    (build-system maven-build-system)
    (arguments
     `(;#:jar-name "maven-deploy-plugin.jar"
                                        ;#:source-dir "src/main/java"
       #:maven-plugins
       (("maven-artifact" ,maven-artifact)
        ("maven-plugin-api" ,maven-plugin-api)
        ,@(default-maven-plugins))
       #:exclude (list "maven-plugin-testing-harness")
       #:tests? #f
       ;#:phases
       ;; (modify-phases %standard-phases
       ;;   (replace 'install
       ;    (install-from-pom "maven-deploy-plugin/pom.xml")
       ))
    (native-inputs
     `(("unzip" ,unzip)))
    (home-page "https://maven.apache.org/plugin-tools/maven-deploy-plugin/")
    (synopsis "Java 5 annotations to use in Mojos")
    (description "This package contains Java 5 annotations for use in Mojos.")
    (license gpl3+)
    )
  )





(define-public jive
  (let ((commit "8bc0b007ec0145d48bd177845e054a608e61f1cd"))
    (package
     (name "jive")
     (version "7.27")
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/tango-controls/jive.git")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "111dyv9rnw06jc7x945x5m8blv3wbjaa46d3022j5ipcv15fa2ay"))
              ))

     (build-system maven-build-system)
     (arguments
      `(
        #:maven-plugins
        (;("maven-deploy-plugin" ,maven-deploy-plugin)
         ,@(default-maven-plugins))
        ;#:exclude (("org.apache.maven.plugins" . ("maven-deploy-plugin")))
        ))
     (synopsis "TANGO distributed control system - Jive")
     (description
      "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
     (home-page "http://www.tango-controls.org/")
     (license gpl3+)
     )))

(define-public tango-controls
  (package
   (name "tango-controls")
   (version "9.3.4")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/tango-controls/TangoSourceDistribution/releases/download/"
                                version
                                "/tango-"
                                version
                                ".tar.gz"))
            (sha256
             (base32
              "0q5s12h67yl0njzasdflc7qmwhkpp3a6jmwx17wszzbd74sl7n2n"))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f
      #:configure-flags
      (list
       "--enable-mariadb"
       "--with-mysql-ho=localhost"
       "--disable-dbcreate"
       (string-append "--with-mariadbclient-lib="
                      (assoc-ref %build-inputs "mariadblib")
                      "/lib")
       (string-append "--with-mariadbclient-include="
                      (assoc-ref %build-inputs "mariadbdev")
                      "/include/mysql")
       (string-append "--with-java="
                      (assoc-ref %build-inputs "icedtea")
                      "/bin/java")
       (string-append "--with-omni="
                      (assoc-ref %build-inputs "omniorb"))
       (string-append "--with-doxygen="
                      (assoc-ref %build-inputs "doxygen")
                      "/bin/doxygen")

       )
      )
    )
   (inputs
    `(("python" ,python)
      ("python2" ,python-2.7)
      ("omniorb" ,omniORB)
      ("mariadbdev" ,mariadb "dev")
      ("mariadblib" ,mariadb "lib")
      ("mariadbout" ,mariadb "out")
      ("curl" ,curl)
      ("glibc" ,glibc)
      ("zlib" ,zlib "out")
      ("zeromq" ,zeromq)
      ("icedtea" ,icedtea-8 "jdk")
      ("doxygen" ,doxygen)
      ("cppzmq" ,cppzmq)))
   (native-inputs
    `(("pkg-config" ,pkg-config)
      ))
   (synopsis "TANGO distributed control system - shared libraries and java tools")
   (description
    "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
   (home-page "http://www.tango-controls.org/")
   (license gpl3+)
   ))
